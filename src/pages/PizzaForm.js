import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const newPizza = document.querySelectorAll('form button');
		newPizza.addEventListener('click', submit);
	}

	submit(event) {
		const val = document.querySelectorAll('form input');
		console.log(val);
	}
}
