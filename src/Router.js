import PizzaForm from './pages/PizzaForm';

export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}

	static #menuElement;
	static set menuElement(element) {
		this.#menuElement = element;
		// au clic sur n'importe quel lien contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
		const tab = element.querySelectorAll('a');
		tab.forEach(element => {
			element.addEventListener('click', e => {
				e.preventDefault();
				Router.navigate(element.getAttribute('href'));
			});
		});
	}
}
