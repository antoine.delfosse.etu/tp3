import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Img from './components/Img';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas

const titre2 = document.querySelectorAll('h4')[1];
console.log(titre2);

const logo = document.querySelector('.logo');
logo.innerHTML += "<small>les pizzas c'est la vie</small>";
console.log(logo);

const lienFooter = document.querySelectorAll('footer a')[1];
console.log(lienFooter);

const laCarte = document.querySelector('.pizzaListLink');
laCarte.setAttribute('class', laCarte.getAttribute('class') + ' active');

const section = document.querySelector('.newsContainer');
section.setAttribute('style', '');

const closeButton = document.querySelector('.closeButton');
function clickClose(event) {
	section.setAttribute('style', 'display:none');
}
closeButton.addEventListener('click', clickClose);

const aboutPage = new Component('section', null, 'Ce site est génial');
const pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');
